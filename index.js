const express = require('express');
const config = require('./config.json')
var mysql = require('mysql');
const app = express();
const port = 3000;

var connection = mysql.createConnection({
    host     : config.db_host,
    user     : config.db_user,
    password : config.db_password,
    database : config.db_database
});

countSQL = "SELECT table_rows 'count' FROM information_schema.tables WHERE table_name='hits' AND table_schema='sitereaper'"
searchTitleSQL = "SELECT * FROM hits WHERE MATCH (title) AGAINST (\"?\" IN NATURAL LANGUAGE MODE) LIMIT 50000"
searchIndex = "SELECT * FROM indexes WHERE hit_id = ? LIMIT 1;"

app.get('/count', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    connection.query(countSQL, function(err, data, fields) {
        if (err) throw err;
        res.json(data[0])
    })
})

app.get('/search', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    connection.query(searchTitleSQL, [req.query.query], function(err, data, fields) {
        if (err) throw err;
        res.json(data)
    })
})

app.get('/index', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    connection.query(searchIndex, [req.query.id], function(err, data, fields) {
        if (err) throw err;
        res.json(data)
    })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})